import requests

# set the base URL and parameters for the API request
base_url = 'https://clinicaltrials.gov/api/query/study_fields?'
params = {
    'expr': '',
    'min_rnk': '1',
    'max_rnk': '1000',
    'fmt': 'JSON',
    'fields': 'NCTId,Condition,InterventionName,LeadSponsorName,StartDate,PrimaryCompletionDate,StudyType,DesignAllocation,DesignInterventionModel,DesignMasking'
}

# prompt the user to enter a search term
search_term = input('Enter a search term: ')

# add the search term to the parameters
params['expr'] = search_term

# encode the parameters
encoded_params = '&'.join([f"{k}={v}" for k, v in params.items()])

# send the API request and check for errors
response = requests.get(base_url + encoded_params)

if response.status_code != 200:
    print(f"Error: API request returned status code {response.status_code}.")
    exit()

# parse the API response and extract the NCT IDs
try:
    data = response.json()
    nct_ids = []
    for study in data['StudyFieldsResponse']['StudyFields']:
        if (study['StudyType'][0].lower() == 'interventional' and
            (not study.get('DesignAllocation', ['']) or 'randomized' in study['DesignAllocation'][0].lower()) and
            (not study.get('DesignInterventionModel', ['']) or 'parallel assignment' in study['DesignInterventionModel'][0].lower()) and
            (not study.get('DesignMasking', ['']) or any(masking in study['DesignMasking'][0] for masking in ['Double', 'Triple', 'Quadruple']))):
            nct_ids.append(study['NCTId'][0])
    print(f"Found {len(nct_ids)} studies matching search term '{search_term}':")
    print(nct_ids)
except ValueError:
    print("Error: API response could not be parsed as JSON.")
    exit()
except KeyError:
    print("Error: API response did not contain expected data.")
    exit()
except IndexError:
    print("Error: list index out of range.")
    exit()
